package com.ronildo.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class CadastroActivity extends AppCompatActivity {

    private EditText name;
    private EditText email;
    private EditText password;
    private EditText repeatPassword;

    public void salvar(View view) {
        if(isValid()) {
            Log.i("INFO", "Salvo com sucesso");
        }

        if(!isValid()) {
            Log.i("INFO", "Falha na validação");
        }
    }

    private boolean isValid() {
        boolean valid = false;

        valid = password.getText().toString().equals(repeatPassword.getText().toString())
                && hasText(password)
                && hasText(name)
                && hasText(email);

        return valid;
    }

    private boolean hasText(EditText editText) {
        return editText.getText().toString().length() > 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        name = findViewById(R.id.editTextName);
        email = findViewById(R.id.editTextEmail);
        password = findViewById(R.id.editTextPassword);
        repeatPassword = findViewById(R.id.editTextRepeatPassword);
    }
}
